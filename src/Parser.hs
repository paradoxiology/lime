{-# LANGUAGE OverloadedStrings #-}

module Parser (
  parseCommand
) where

import qualified Data.Text as T

import Text.Parsec hiding ((<|>), many)
import Text.Parsec.Text
import Control.Applicative hiding (optional)

import Range
import Command

integer :: Parser Int
integer = do
  sign <- option ' ' (char '-')
  num  <- many1 digit
  return $ read (sign : num)

range :: Parser Range
range = option fullRange (try lRange <|> atRange)
  where
    atRange = do
      a <- integer
      return $ mkRangeAt a
    lRange = do
      f <- optionMaybe integer
      r <- char ',' *> optionMaybe integer
      case (f, r) of
        (Just from, Just to) -> return $ mkRange from to
        (Just from, Nothing) -> return $ mkRangeFrom from
        (Nothing, Just to) -> return $ mkRangeTo to
        (Nothing, Nothing) -> return fullRange

regex :: Parser T.Text
regex = do
  re <- char '/' *> manyTill anyChar (try (char '/'))
  return $ T.pack re

lnCmd :: Parser LineCommand
lnCmd =  char 'p' *> return PrintLines
     <|> char 'd' *> return DelLines

fdCmd :: Parser FieldCommand
fdCmd = do
  r <- (char '$' <|> char ':') *> range
  return $ PrintFields r

cmd :: Parser Command
cmd = do
  lr <- range -- Pre-regex line range
  re <- optionMaybe regex -- Regex filter
  rer <- range -- Post Regex line range
  c <- lnCmd -- Line command
  fcs <- case c of
    PrintLines -> optionMaybe $ many1 fdCmd -- Field command list
    DelLines -> if rer == fullRange
                  then (oneOf ":$" *> unexpected "field specifiers for delete command.") <|> return Nothing
                  else fail "Regex range is not allowed for line deletion command."
  eof
  return $ Command (LineSelection lr re rer) c " " fcs

parseCommand :: T.Text -> Either ParseError Command
parseCommand = parse cmd ""

