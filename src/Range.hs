module Range (
  Range,
  fullRange,
  mkRange,
  mkRangeFrom,
  mkRangeTo,
  mkRangeAt,
  takeRange,
  dropRange
) where

data RangePos
  = RangeStart
  | RangeEnd
  | RangeMiddle !Int
  deriving (Show, Eq)

type Range = (RangePos, RangePos)

fullRange ::  (RangePos, RangePos)
fullRange = (RangeStart, RangeEnd)

mkRange ::  Int -> Int -> (RangePos, RangePos)
mkRange from to = (RangeMiddle from, RangeMiddle to)

mkRangeFrom ::  Int -> (RangePos, RangePos)
mkRangeFrom f = (RangeMiddle f, RangeEnd)

mkRangeTo ::  Int -> (RangePos, RangePos)
mkRangeTo t = (RangeStart, RangeMiddle t)

mkRangeAt ::  Int -> (RangePos, RangePos)
-- Special case for the last element
mkRangeAt (-1) = (RangeMiddle (-1), RangeEnd)

mkRangeAt a = (RangeMiddle a, RangeMiddle (a + 1))

-- List indexing for negative index
at ::  [a] -> Int -> Int
at l i = if i < 0 then length l + i else i

takeRange :: Range -> [a] -> [a]
takeRange r l
  = case r of
    (RangeStart, RangeEnd) -> l
    (RangeMiddle f, RangeEnd) -> drop (l `at` f) l
    (RangeMiddle f, RangeMiddle t) -> drop (l `at` f) $ take (l `at` t) l
    (RangeStart, RangeMiddle t) -> take (l `at` t) l

dropRange :: Range -> [a] -> [a]
dropRange r l
  = case r of
    (RangeStart, RangeEnd) -> []
    (RangeMiddle f, RangeEnd) -> take (l `at` f) l
    (RangeMiddle f, RangeMiddle t) -> take (l `at` f) l ++ drop (l `at` t) l
    (RangeStart, RangeMiddle t) -> drop (l `at` t) l

