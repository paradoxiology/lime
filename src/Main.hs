{-# LANGUAGE DeriveDataTypeable #-}

import System.Console.CmdArgs

import Data.Maybe (fromMaybe)

import qualified Data.Text as T
import qualified Data.Text.IO as T

import Data.Version (showVersion)

import Paths_Lime (version)


import Parser
import Command

data Lime = Lime
  { delimiter :: Maybe String
  , command   :: String
  }
  deriving (Data, Typeable)

lime :: Lime
lime = Lime
  { delimiter = def &= help "Field delimiter"
  , command = def &= args &= typ "Command" 
  } &=
  help "LIne Manipulation Engine" &=
  summary ("Lime v" ++ showVersion version ++ ", (C) Paradoxiology")

main ::  IO ()
main = do
  ops <- cmdArgs lime
  let del = fromMaybe " " (delimiter ops)
  let parseResult = parseCommand (T.pack $ command ops)
  case parseResult of
    Left e    -> print e
    Right cmd -> do
      stdinContent <- T.getContents
      T.putStr $ execute (cmd {fieldDelimiter = T.pack del}) stdinContent
