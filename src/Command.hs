{-# LANGUAGE OverloadedStrings #-}

module Command (
  Command (..),
  LineSelection (..),
  LineCommand (..),
  FieldCommand (..),
  execute
) where

import qualified Data.Text as T

import Text.Regex.TDFA ((=~))
import Text.Regex.TDFA.Text ()

import Range

data LineSelection = LineSelection
  { rangeSel      :: !Range -- Pre-regex range selection
  , regexSel      :: !(Maybe T.Text)
  , regexRangeSel :: !Range -- Post-regex range selection
  } deriving (Show)

data LineCommand
  = PrintLines
  | DelLines
  deriving (Show)

data FieldCommand = PrintFields !Range deriving (Show)

type TxtDelimiter = T.Text

data Command = Command
  { lineSel        :: !LineSelection
  , lineCmd        :: !LineCommand
  , fieldDelimiter :: !TxtDelimiter
  , fieldCmds      :: !(Maybe [FieldCommand])
  } deriving (Show)

execute ::  Command -> T.Text -> T.Text
execute cmd text
  = let selectedLines = seletLines (lineCmd cmd) (lineSel cmd) (T.lines text) in
          case fieldCmds cmd of
            Just fcs -> T.unlines $ map (execFieldCommands fcs (fieldDelimiter cmd)) selectedLines
            Nothing  -> T.unlines selectedLines

seletLines :: LineCommand -> LineSelection -> [T.Text] -> [T.Text]
seletLines lnCmd lnSel
  = case regexSel lnSel of
      Just regex -> takeRange (regexRangeSel lnSel)
                    . execRegexCommand lnCmd regex
                    . takeRange (rangeSel lnSel)
      Nothing -> execLineCommand lnCmd (rangeSel lnSel)

execLineCommand :: LineCommand -> Range -> [a] -> [a]
execLineCommand PrintLines r = takeRange r
execLineCommand DelLines r = dropRange r

execRegexCommand :: LineCommand -> T.Text -> [T.Text] -> [T.Text]
execRegexCommand PrintLines regex = if T.null regex then id else filter (=~ regex)
execRegexCommand DelLines regex = if T.null regex then id else filter (not . (=~ regex))

execFieldCommands :: [FieldCommand] -> TxtDelimiter -> T.Text -> T.Text
execFieldCommands fcs fd text = patchedJoin (concatMap execFdCmd fcs)
  where
     fields = tokeniseText fd text
     execFdCmd (PrintFields fieldRange) = takeRange fieldRange fields
     join = T.intercalate fd
     -- Prepend the delimiter if necessary
     patchedJoin fs = if length fs > 1 && not (T.null text) && T.isPrefixOf fd text
                        then T.append fd (join fs)
                        else join fs

tokeniseText :: TxtDelimiter -> T.Text -> [T.Text]
tokeniseText delimiter = filterEmptyText . splitText
  where
    splitText = T.splitOn delimiter
    filterEmptyText = filter (/= "")

